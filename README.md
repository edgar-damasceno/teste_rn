# TESTE FULLSTACK JAVASCRIPT | Mobile e Web | MOBILE SAÚDE #

O objetivo desse teste é testar o candidato nas stacks exigidas para a vaga de fullstack javascript.

### O que será analisado? ###

* Análise e compreensão do escopo
* Lógica e domínio sobre a liguagem JavaScript
* Lógica e domínio sobre o framework React Native, Express e Angular
* Compreensão de modelos de dados e modelagem de novas estruturas
* Conhecimento de Redis e ElasticSearch
* Organização do projeto e do código
* Desenvoltura com arquitetura baseada em microserviços
* Desenvoltura para lidar com sistemas em produção e criação de novas funcionalidades
* Performance e velocidade de entrega
* Qualidade da entrega

### Resumo do escopo ###
* Fazer um Fork do projeto base do teste (https://bitbucket.org/mobilesaude2/teste_rn/src)
* Corrigir um bug proposital existente em um dos EndPoints da API do Projeto Base
* Modelar um conjunto de novas tabelas no banco para cadastro de usuários e planos de saúde
* Criar as APIs REST com um crud para alimentar as tabelas criadas
* Implementar endpoints para registrar e consultar chamados no ElasticSearch
* De acordo com o layout já iniciado, listar os chamados recuperados;
* Implementar uma tela para adição de um novo chamado - web e mobile
* Adicionar ao projeto um dump do banco de dados que foi modelado
* Adicionar ao projeto uma build do projeto mobile
* Entregar uma Collection com todos os EndPoints em perfeito funcionamento
* Abrir um PULL Request do projeto editado por você

## Parte 1 - Configurar acessos do projeto
* Você receberá no seu email informações para configurar a conexão com o ElasticSearch e a porta em que a API deverá rodar;

## Parte 2 - Corrigir bug na API
* O projeto possui uma API que está com um erro proposital, encontre e corrija o erro na API;

## Parte 3 - Modelar o banco MySQL 8
* Criar uma tabela de planos com os campos: id e descricao;
* Criar uma tabela de pacientes com os campos: id, cpf, nome, data_nascimento, sexo, telefone, id_plano;

## Parte 4 - Contruir API
* Adicionar ao projeto uma API REST com CRUD completo para a tabela de planos, tendo como rota base `/plano`;
* Adicionar ao projeto uma API REST com CRUD completo para a tabela de pacientes, tendo como rota base `/paciente`;
* No endpoint de carregar um paciente retornar também a descrição do plano vinculado ao mesmo;
* Criar um endpoint que registra um chamado novo no ElasticSearch;
* Usar o endpoint que lista pacientes para dar opção de selecionar um paciente na hora de inserir um chamado;
* Usar o endpoint que lista motivos (que estava com um bug proposital) para dar a opção de selecionar um motivo ao inserir o chamado;
* Campos de um chamado:
    * id_paciente
    * nome_paciente
    * id_motivo
    * numero_chamado
    * descricao
    * status
    * data_criacao
* Criar um endpoint que busca e lista os chamados no ElasticSearch, com controles de paginação e busca, o retorno do endpoint deve atender ao esperado pelo frontend;

## Parte 5 - Front
* Criar camada de serviços para trabalhar com a API de Chamados;
* De acordo com o layout já iniciado, listar os chamados recuperados;
* Caso necessário, fique a vontade para alterar os campos do grid de chamados;
* Adicionar um botão e funcionalidade para criar um Novo Chamado na página de chamados;
* Implementar funcionalidade para editar e visualizar os detalhes de um chamado selecionado;
* Implementar busca de chamado;
* Implementar paginação do grid de chamados;
* O Angular está na versão 6, as funcionalidades deverão ser implementadas utilizando esta versão;
* Vulnerabilidades das dependências não fazem parte do objetivo deste teste.
* Observação: Caso prefirir, a parte de front pode ser entregue em Vue ao invés de angular.
    * Imagem para referência ![](https://omnihomolog.mobilesaude.com.br/v1/static/teste/lista_chamados.png)

## Passo 6 - Front Mobile
* Criar camada de serviços para trabalhar com a API de Chamados;
* Use o Redux para controlar o estado da aplicação.
* De acordo com o layout já iniciado, listar os chamados recuperados;
* Caso necessário, fique a vontade para alterar os campos da lista de chamados;
* Adicionar um botão e funcionalidade para criar um Novo Chamado na página de chamados;
* Implementar funcionalidade para editar e visualizar os detalhes de um chamado selecionado;
* Implementar busca de chamado;
* Implementar paginação da lista de chamados;
* Fique a vontade para utilizar components de classe ou função.

## Parte 7 - Entrega
* Teste todos os requisitos para uma entrega com qualidade. Este ponto será avaliado pelo nosso time;
* Realizar o Pull Request do seu projeto javascript fullstack;
* Disponibilizar uma Collection do Postman com exemplos de uso das APIs criadas por você;
* Se desejar, descreva brevemente o critérios adotados por você no seu projeto;
* Gerar o apk de release (android)
